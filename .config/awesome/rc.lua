-- dotmgr
----use not root@* server
----end

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "/theme.lua")

-- This is used later as the default terminal and editor to run.
primary = 1                 -- left
secondary = screen.count()  -- right
terminal = "alacritty"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
--    awful.layout.suit.tile.top,
--    awful.layout.suit.fair,
--    awful.layout.suit.fair.horizontal,
--    awful.layout.suit.spiral,
--    awful.layout.suit.spiral.dwindle,
--    awful.layout.suit.max,
--    awful.layout.suit.max.fullscreen,
--    awful.layout.suit.magnifier
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
local mylayouts = {}
mylayouts[primary] = {
    awful.layout.layouts[1],
    awful.layout.layouts[1],
    awful.layout.layouts[3],
    awful.layout.layouts[3],
    awful.layout.layouts[2],
    awful.layout.layouts[2],
    awful.layout.layouts[2],
    awful.layout.layouts[3],
    awful.layout.layouts[4],
}
mylayouts[secondary] = {
    awful.layout.layouts[1],
    awful.layout.layouts[1],
    awful.layout.layouts[2],
    awful.layout.layouts[2],
    awful.layout.layouts[3],
    awful.layout.layouts[3],
    awful.layout.layouts[3],
    awful.layout.layouts[2],
    awful.layout.layouts[4],
}
local mylayouts_l = {
    awful.layout.layouts[1],
    awful.layout.layouts[1],
    awful.layout.layouts[3],
    awful.layout.layouts[3],
    awful.layout.layouts[2],
    awful.layout.layouts[2],
    awful.layout.layouts[3],
    awful.layout.layouts[3],
    awful.layout.layouts[4],
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              -- Without this, the following
                                              -- :isvisible() makes no sense
                                              c.minimized = false
                                              if not c:isvisible() and c.first_tag then
                                                  c.first_tag:view_only()
                                              end
                                              -- This will also un-minimize
                                              -- the client, if needed
                                              client.focus = c
                                              c:raise()
                                          end),
                     awful.button({ }, 2, client_menu_toggle_fn()),
                     awful.button({ }, 3, function (c)
                                              c.minimized = not c.minimized
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, mylayouts[s.index])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    tray = wibox.widget.systray()
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            tray,
            mytextclock,
            s.mylayoutbox,
        },
    }
    if s.index == secondary then
        tray:set_screen(s)
    end

    -- Display initial tags
    if s.index == secondary then
        s.tags[9]:view_only()
    else
        s.tags[8]:view_only()
    end
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    -- Awesome
    awful.key({ modkey,           },          "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey            }, "x",      function () awful.screen.focused().mypromptbox:run()                       end,
              {description = "run prompt", group = "awesome"}),
    awful.key({ modkey            }, "z",      function () mymainmenu:show({keygrabber=true})                             end,
              {description = "show main menu", group = "awesome"}),

----only fluxcapacitor
    -- MPD controls
    awful.key({ modkey            }, "F12",    function () awful.util.spawn_with_shell("ssh tweety mpc volume +5")        end,
              {description = "raise jukebox volume", group = "media"}),
    awful.key({ modkey            }, "F9",     function () awful.util.spawn_with_shell("ssh tweety mpc volume -5")        end,
              {description = "lower jukebox volume", group = "media"}),
    awful.key({                   }, "Pause",  function () awful.util.spawn_with_shell("ssh tweety mpc toggle")           end,
              {description = "toggle jukebox playback", group = "media"}),
    awful.key({ modkey            }, "F10",    function () awful.util.spawn_with_shell("ssh tweety mpc prev")             end,
              {description = "jump to previous song on jukebox", group = "media"}),
    awful.key({ modkey            }, "F11",    function () awful.util.spawn_with_shell("ssh tweety mpc next")             end,
              {description = "jump to next song on jukebox", group = "media"}),
----only vger
    -- XF86 keys (defunct -> using mod+Fx)
    awful.key({ modkey }, "F5",                function () awful.util.spawn("light -U 10")                                end,
              {description = "raise screen brightness", group = "media"}),
    awful.key({ modkey }, "F6",                function () awful.util.spawn("light -A 10")                                end,
              {description = "lower screen brightness", group = "media"}),
    -- Volume controls
    awful.key({}, "XF86AudioRaiseVolume",      function () awful.util.spawn_with_shell("pactl set-sink-volume 0 +5%")     end,
              {description = "raise audio volume", group = "media"}),
    awful.key({}, "XF86AudioLowerVolume",      function () awful.util.spawn_with_shell("pactl set-sink-volume 0 -5%")     end,
              {description = "lower audio volume", group = "media"}),
    awful.key({}, "XF86AudioMute",             function () awful.util.spawn_with_shell("pactl set-sink-mute 0 toggle")    end,
              {description = "toggle mute audio", group = "media"}),
----end

    -- Movement
    awful.key({ modkey,           }, "k",      function () awful.client.focus.bydirection("up")                           end,
              {description = "focus client above", group = "movement"}),
    awful.key({ modkey,           }, "j",      function () awful.client.focus.bydirection("down")                         end,
              {description = "focus client below", group = "movement"}),
    awful.key({ modkey,           }, "h",      function () awful.client.focus.bydirection("left")                         end,
              {description = "focus client to the left", group = "movement"}),
    awful.key({ modkey,           }, "l",      function () awful.client.focus.bydirection("right")                        end,
              {description = "focus client to the right", group = "movement"}),
    awful.key({ modkey,           }, "[",      function () awful.screen.focus_bydirection("left")                         end,
              {description = "focus the screen to the left", group = "movement"}),
    awful.key({ modkey,           }, "]",      function () awful.screen.focus_bydirection("right")                        end,
              {description = "focus the screen to the right", group = "movement"}),
    awful.key({ modkey,           }, "Return", function () client.focus:swap(awful.client.getmaster())                    end,
              {description = "focus master", group = "movement"}),

    awful.key({ modkey, "Shift"   }, "k",      function () awful.client.swap.bydirection("up")                            end,
              {description = "swap with client above", group = "client"}),
    awful.key({ modkey, "Shift"   }, "j",      function () awful.client.swap.bydirection("down")                          end,
              {description = "swap with client below", group = "client"}),
    awful.key({ modkey, "Shift"   }, "h",      function () awful.client.swap.bydirection("left")                          end,
              {description = "swap with client to the left", group = "client"}),
    awful.key({ modkey, "Shift"   }, "l",      function () awful.client.swap.bydirection("right")                         end,
              {description = "swap with client to the right", group = "client"}),
    awful.key({ modkey, "Shift"   }, "[",      function () awful.client.movetoscreen(client.focus, primary)               end,
              {description = "move client to the left screen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "]",      function () awful.client.movetoscreen(client.focus, secondary)             end,
              {description = "move client to the right screen", group = "client"}),

    -- Layout
    awful.key({ modkey,           }, ",",      function () awful.tag.incnmaster( 1)                                       end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey,           }, ".",      function () awful.tag.incnmaster(-1)                                       end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, ",",      function () awful.tag.incncol(-1)                                          end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, ".",      function () awful.tag.incncol( 1)                                          end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "Tab",    function () awful.layout.inc( 1, awful.screen.focused())                   end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Control" }, "Tab",    function () awful.layout.inc(-1, awful.screen.focused())                   end,
              {description = "select previous", group = "layout"}),

    -- Launchers
    awful.key({ modkey, "Control" }, "a",      function () awful.util.spawn("audacity")                                   end,
              {description = "open Audacity", group = "launcher"}),
    awful.key({ modkey, "Control" }, "b",      function () awful.util.spawn("blender")                                    end,
              {description = "open Blender", group = "launcher"}),
    awful.key({ modkey, "Control" }, "e",      function () awful.util.spawn("eclipse")                                    end,
              {description = "open Eclipse", group = "launcher"}),
    awful.key({ modkey, "Control" }, "g",      function () awful.util.spawn("gimp")                                       end,
              {description = "open GIMP", group = "launcher"}),
    awful.key({ modkey, "Control" }, "i",      function () awful.util.spawn("element-desktop")                            end,
              {description = "open IM client", group = "launcher"}),
    awful.key({ modkey, "Control" }, "m",      function () awful.util.spawn("thunderbird")                                end,
              {description = "open mail client", group = "launcher"}),
    awful.key({ modkey, "Control" }, "n",      function () awful.util.spawn("inkscape")                                   end,
              {description = "open inkscape", group = "launcher"}),
    awful.key({ modkey, "Control" }, "o",      function () awful.util.spawn("octave --force-gui")                         end,
              {description = "open octave", group = "launcher"}),
    awful.key({ modkey, "Control" }, "p",      function () awful.util.spawn("qtpass")                                     end,
              {description = "open password manager", group = "launcher"}),
    awful.key({ modkey, "Control" }, "s",      function () awful.util.spawn("libreoffice")                                end,
              {description = "open office solution", group = "launcher"}),
    awful.key({ modkey, "Control" }, "v",      function () awful.util.spawn("virtualbox")                                 end,
              {description = "open virtual machine manager", group = "launcher"}),
    awful.key({ modkey, "Control" }, "w",      function () awful.util.spawn("firefox")                                    end,
              {description = "open webbrowser", group = "launcher"}),
    awful.key({ modkey, "Control" }, "Down",   function () awful.util.spawn_with_shell("xmodmap .Xmodmap")                end,
              {description = "reload .Xmodmap", group = "launcher"}),
    awful.key({ modkey, "Control" }, "Right",  function () awful.util.spawn_with_shell("setxkbmap de; xmodmap .Xmodmap")  end,
              {description = "set DE keyboard layout", group = "launcher"}),
    awful.key({ modkey, "Control" }, "Left",   function () awful.util.spawn_with_shell("setxkbmap us; xmodmap .Xmodmap")  end,
              {description = "set US keyboard layout", group = "launcher"}),
    awful.key({ modkey, "Control" }, "Return", function () awful.util.spawn(terminal)                                     end,
              {description = "open a terminal", group = "launcher"})
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "BackSpace",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey,           }, "Escape", function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey,           }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey,           }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey, "Control" }, "BackSpace",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     screen = function (c) return awesome.startup and c.screen or awful.screen.focused() end,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },

    { rule = { class = "mpv" },                         properties = { floating = true } },
    { rule = { class = "Vncviewer" },                   callback = awful.placement.centered },

    { rule = { class = "Eclipse" },                     properties = { screen = primary,   tag = "1" } },

    { rule = { class = "firefox" },                     properties = { screen = primary,   tag = "5" } },
    { rule = { class = "Element" },                     properties = { screen = secondary, tag = "6" } },
    { rule = { class = "Signal" },                      properties = { screen = secondary, tag = "6" } },

    { rule = { class = "thunderbird" },                 properties = { screen = secondary, tag = "7" } },

    { rule = { instance = "defterm_l" },                properties = { screen = primary,   tag = "8" } },
    { rule = { instance = "defterm_r" },                properties = { screen = secondary, tag = "8" } },

    { rule = { class = "XConsole" },                    properties = { screen = secondary, tag = "9" } },
    { rule = { class = "XOsview" },                     properties = { screen = secondary, tag = "9" } },
    { rule = { instance = "sysmon" },                   properties = { screen = secondary, tag = "9" } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
