# vim: filetype=gitconfig
[core]
    sshCommand = "ssh -i ~/.ssh/id_rsa-pizzakatze"

[user]
	name = PizZaKatZe
	email = pizzakatze@chaos-siegen.de
