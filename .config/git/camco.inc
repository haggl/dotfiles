# vim: filetype=gitconfig
[core]
    sshCommand = "ssh -i ~/.ssh/id_rsa-sneuser"

[user]
	name = Sebastian Neuser
	email = sebastian.neuser@camco.de
