/*
 * Copyright TODO Sebastian Neuser
 *
 * This file is part of TODO.
 *
 * TODO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TODO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TODO.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * TODO
 * @author Sebastian Neuser
*/

#ifndef _TODO_H
#define _TODO_H


//---------------- includes ----------------//
#include <stdint.h>


//---------------- constants ----------------//


//---------------- data types ----------------//


//---------------- functions and procedures ----------------//


//---------------- EOF ----------------//
#endif // _TODO_H
