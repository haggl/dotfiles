#!/usr/bin/env python3
# Copyright TODO Sebastian Neuser
#
# This file is part of TODO.
#
# TODO is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TODO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TODO.  If not, see <http://www.gnu.org/licenses/>.
"""TODO

Detailed description.
"""

from argparse import ArgumentParser

def parse_cmdline():
    """Parses command line arguments given to the script.

    Returns:
        the command line arguments parsed by argparse
    """
    parser = ArgumentParser(description=globals()['__doc__'])
    parser.add_argument('--long-opt', dest='long_opt', default='foobar',
                        help='a helpful string')
    parser.add_argument('file', help='such helpful')
    return parser.parse_args()


def main():
    """Program entry point.
    """
    args = parse_cmdline()
    print(args)

if __name__ == "__main__":
    main()
