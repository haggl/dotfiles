#!/bin/sh
# WHATIS
# @author Sebastian Neuser


# ---  V A R I A B L E S  --- #

SCRIPT_NAME=$(basename "$0")



# ---  S U B R O U T I N E S  --- #

print_help_message_and_exit() {
    cat << EOM
usage: $SCRIPT_NAME [-h]
       $SCRIPT_NAME ARG

WHATIS

options:
  -h        show this help message

positional arguments:
  ARG       DESCRIPTION
EOM
    exit
}



# ---  M A I N   S C R I P T  --- #

while getopts "h" option; do
    case $option in
        *) print_help_message_and_exit
    esac
done
shift $((OPTIND - 1))

if [ -z "$1" ]; then
    print_help_message_and_exit
fi
