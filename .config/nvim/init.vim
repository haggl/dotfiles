" SETTINGS
" general
set noswf
set modeline
set exrc
set secure
set number
set relativenumber
set mouse=

" colors and highlighting
syn on
set hlsearch

if &diff
    colors default
else
    colors evening
endif

highlight SpaceError    ctermbg=brown
highlight LengthInfo    ctermbg=green
highlight LengthWarn    ctermbg=yellow
highlight LengthError   ctermbg=red

let g:highlight_enabled  = 0
let g:match_length_info  = 0
let g:match_length_warn  = 0
let g:match_length_error = 0
let g:match_space_error  = 0
function! ToggleHighlight()
    if g:highlight_enabled
        let g:highlight_enabled = 0
        call matchdelete(g:match_length_info)
        call matchdelete(g:match_length_warn)
        call matchdelete(g:match_length_error)
        call matchdelete(g:match_space_error)
    else
        let g:highlight_enabled  = 1
        let g:match_length_info  = matchadd('LengthInfo',  '\%81v.',  -1)
        let g:match_length_warn  = matchadd('LengthWarn',  '\%101v.', -1)
        let g:match_length_error = matchadd('LengthError', '\%121v.', -1)
        let g:match_space_error  = matchadd('SpaceError',  '\( \+\t\)\|\(\s\+$\)', -1)
    endif
endfunction

autocmd! InsertEnter,InsertLeave * highlight SpaceError  ctermbg=brown
                                \| highlight LengthInfo  ctermbg=green
                                \| highlight LengthWarn  ctermbg=yellow
                                \| highlight LengthError ctermbg=red

" cursorline
set cursorline
autocmd! WinEnter * set cursorline
autocmd! WinLeave * set nocursorline

" indentation
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set cindent
set cinkeys-=0#
set indentkeys-=0#

" line wrapping
let g:linebreak_enabled = 1
function! ToggleLinebreak()
    let g:linebreak_enabled = !g:linebreak_enabled
    if g:linebreak_enabled
        set linebreak
        set showbreak=..
    else
        set nolinebreak
        set showbreak=
    endif
endfunction

set linebreak
set breakindent
set breakindentopt=sbr
set showbreak=..


" SYNTASTIC PLUGIN
if !&diff
    set statusline=%t       "tail of the filename
    set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
    set statusline+=%{&ff}] "file format
    set statusline+=%h      "help file flag
    set statusline+=%m      "modified flag
    set statusline+=%r      "read only flag
    set statusline+=%y      "filetype
    set statusline+=%=      "left/right separator
    set statusline+=%c,     "cursor column
    set statusline+=%l/%L   "cursor line/total lines
    set statusline+=\ %P    "percent through file
    "set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*
    let g:syntastic_always_populate_loc_list = 1
    let g:syntastic_auto_loc_list = 1
    let g:syntastic_check_on_open = 1
    let g:syntastic_check_on_wq = 0
endif


" KEY MAPPINGS
" disable annoying command line window
map  q:                 :
map  q/                 /
map  q?                 ?

" general
map  <F1>               :TlistToggle<CR>
map  <F2>               :set relativenumber!<CR>
map  <F14>              :set number!<CR>
map  <F3>               :set list!<CR>
map  <F15>              :call ToggleLinebreak()<CR>
map  <F4>               :set expandtab!<CR>
map  <F5>               :set spell!<CR>
map  <F6>               :set spelllang=en<CR>
map  <F7>               :set spelllang=de_20<CR>
map  <F8>               :call ToggleHighlight()<CR>
map! <F1>               <Esc>:TlistToggle<CR>a
map! <F2>               <Esc>:set relativenumber!<CR>a
map! <F14>              <Esc>:set number!<CR>a
map! <F15>              <Esc>:call ToggleLinebreak()<CR>a
map! <F3>               <Esc>:set list!<CR>a
map! <F4>               <Esc>:set expandtab!<CR>a
map! <F5>               <Esc>:set spell!<CR>a
map! <F8>               <Esc>:call ToggleHighlight()<CR>a
nmap <Tab>              i<Tab><Esc>w
nmap <Space>            i <Esc>
nmap <Backspace>        :!
nmap <Return>           :

" tabs
map  <Insert>           :tabnew<CR>:e 
map  <Del>              :tabclose<CR>
map  <Home>             :tabfirst<CR>
map  <End>              :tablast<CR>
map  <PageDown>         :tabnext<CR>
map  <PageUp>           :tabprevious<CR>

" buffers
map `                   :ls<CR>:b

" git vimdiff
map <C-Left>            :diffget 1<CR>
map <C-Up>              :diffget 2<CR>
map <C-Right>           :diffget 3<CR>
map <C-Down>            :diffput 4<CR>

" TEMPLATES AND CODE MACROS
" C
autocmd! BufNewFile *.c 0r ~/.config/nvim/template/template.c
autocmd! BufNewFile *.h 0r ~/.config/nvim/template/template.h
autocmd! FileType c let @c="0i//\ej"
                 \| let @d="oDEBUG1; //TODO\e"
                 \| let @e="oelse {\n}\eO"
                 \| let @f="oint i;\nfor (i=0; i < ; ++i) {\n}\ek4Wi"
                 \| let @i="oif () {\n}\ekWa"
                 \| let @s="oswitch () {\ndefault:\n\tbreak;\n}\ekkkWa"
                 \| let @u='02xj'
                 \| let @w="owhile () {\n}\ekWa"
                 \| map <F11> :wa<CR>:!clear; make all<CR>
                 \| imap <F11> <Esc>:wa<CR>:!clear; make all<CR>
                 \| map <F12> :wa<CR>:!clear; make clean compile<CR>
                 \| imap <F12> <Esc>:wa<CR>:!clear; make clean compile<CR>

" Git commit messages
autocmd! FileType gitcommit set textwidth=72

" GPL
autocmd! BufNewFile COPYING 0r ~/.config/nvim/template/COPYING
autocmd! BufEnter COPYING wq

" Haskell
autocmd! BufNewFile *.hs 0r ~/.config/nvim/template/template.hs
autocmd! FileType haskell let @c="0i--\ej"
                       \| let @u="0xxj"
                       \| let b:syntastic_checkers = ["hlint"]

" LaTeX
autocmd! BufNewFile article.tex 0r ~/.config/nvim/template/article.tex
autocmd! BufNewFile beamer.tex 0r ~/.config/nvim/template/beamer.tex
autocmd! BufNewFile letter.tex 0r ~/.config/nvim/template/letter.tex
autocmd! BufNewFile macros.tex 0r ~/.config/nvim/template/macros.tex
autocmd! BufNewFile protokoll.tex 0r ~/.config/nvim/template/protokoll.tex
autocmd! BufNewFile section.tex 0r ~/.config/nvim/template/section.tex
autocmd! FileType tex let @a="o\\begin{align}\n\\end{align}\eO\t"
                   \| let @b="o\n\\subsubsection{}\ei"
                   \| let @c="0i%\ej"
                   \| let @d="o\\begin{description}\n\\end{description}\eO\t"
                   \| let @e="o\\begin{equation}\n\\end{equation}\eO\t"
                   \| let @g="i\\unit{\eEa}{\elxEa}\e"
                   \| let @i="\^i\\item \ej"
                   \| let @l="a\b\""
                   \| let @n="o\\begin{enumerate}\n\\end{enumerate}\eO"
                   \| let @p="o\\paragraph{}\ei"
                   \| let @s="o\n\n\n\\subsection{} \\label{sect:}\eBhhi"
                   \| let @t="o\\begin{itemize}\n\\end{itemize}\eO"
                   \| let @u="0xj"
                   \| map  <F9>  :wa<CR>:!zathura `hitex.pl -N`.pdf &> /dev/null &<CR>
                   \| map  <F10> :wa<CR>:!okular `hitex.pl -N`.pdf &> /dev/null &<CR>
                   \| map  <F11> :wa<CR>:!hitex.pl -Aa<CR>
                   \| map  <F12> :wa<CR>:!hitex.pl -A<CR>
                   \| imap <F11> <Esc>:wa<CR>:!hitex.pl -Aa<CR>a
                   \| imap <F12> <Esc>:wa<CR>:!hitex.pl -A<CR>a
                   \| let b:syntastic_checkers = ["latex"]

" Emails
autocmd! BufNewFile,BufRead *.mail,/tmp/evo?????? set filetype=mail
autocmd! FileType mail set textwidth=72
                    \| set nocindent
                    \| set norelativenumber
                    \| set nonumber

" Markdown
autocmd! FileType markdown set tabstop=2
                        \| set shiftwidth=2
                        \| map  <F12> :wa<CR>:execute '!clear; git add %; git commit --fixup=HEAD; git push'<CR>
                        \| imap <F12> <Esc>:wa<CR>:execute '!clear; git add %; git commit --fixup=HEAD; git push'<CR><CR>a

" Octave/Matlab
autocmd! BufNewFile *.m 0r ~/.config/nvim/template/template.m
autocmd! FileType octave let @c="0i%\ej"
                      \| let @u="0xj"

" Perl
autocmd! BufNewFile *.pl 0r ~/.config/nvim/template/template.pl
autocmd! FileType perl let @c="0i#\ej"
                    \| let @u="0xj"

" PlantUML
autocmd! BufNewFile *.pu map  <F9>  :!feh --reload 1 %:r.png &> /dev/null &<CR><CR>
                      \| map  <F12> :w<CR>:!plantuml %<CR><CR>
                      \| imap <F12> <Esc>:w<CR>:!plantuml %<CR><CR>a
autocmd! BufRead *.pu map  <F9>  :!feh --reload 1 %:r.png &> /dev/null &<CR><CR>
                   \| map  <F12> :w<CR>:!plantuml %<CR><CR>
                   \| imap <F12> <Esc>:w<CR>:!plantuml %<CR><CR>a

" Python
autocmd! BufNewFile *.py 0r ~/.config/nvim/template/template.py
autocmd! FileType python let @c="0i#\ej"
                      \| let @u="0xj"

" (ba)sh
autocmd! BufNewFile *.sh 0r ~/.config/nvim/template/template.sh

" Supercollider plugin configuration
autocmd! BufNewFile,BufRead *.sc,*.scd set filetype=supercollider
autocmd! FileType supercollider let @c="0i//\ej"
                             \| let @u='02xj'
                             \| set shiftwidth=4
                             \| set tabstop=4

" Tidal Cycles plugin configuration
autocmd! BufEnter,BufNewFile,BufRead *.tidal set filetype=tidal
autocmd! FileType tidal packadd vim-tidal
                     \| let b:tidal_config = { "socket_name": "default", "target_pane": "PZKZ:2.1" }
                     \| set shiftwidth=2
                     \| set tabstop=2

" YAML
autocmd! BufNewFile *.yml 0r ~/.config/nvim/template/template.yml
autocmd! FileType yaml let @c="0i#\ej"
                    \| let @u="0xj"
                    \| set shiftwidth=2
                    \| set tabstop=2
