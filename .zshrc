# S E T T I N G S
# completion
export FPATH="$HOME/.config/zsh/completions:$FPATH"
autoload -Uz compinit
compinit
zstyle ':completion:*' completer _expand _complete _approximate _ignored
zstyle ':completion:*' menu select
zstyle ':completion:*' special-dirs true
zstyle ':completion:*:commands' rehash 1
zstyle :compinstall filename "$HOME/.zshrc"
setopt COMPLETE_ALIASES

# history
HISTFILE=~/.histfile
SAVEHIST=4000
HISTSIZE=4000
setopt SHARE_HISTORY HIST_FIND_NO_DUPS

# behavior
setopt AUTOCD NOTIFY
unsetopt BEEP

# vi mode
function zvm_config() {
    ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT
}

function zvm_after_init() {
    # Append your custom color for your cursor
    ZVM_NORMAL_MODE_CURSOR=$(zvm_cursor_style $ZVM_CURSOR_BLOCK)'\e\e]12;#aa0000\a'
    ZVM_INSERT_MODE_CURSOR=$(zvm_cursor_style $ZVM_CURSOR_BLINKING_BEAM)'\e\e]12;#228822\a'
}
source /usr/share/zsh/plugins/zsh-vi-mode/zsh-vi-mode.zsh


# E N V I R O N M E N T
export EDITOR="nvim"
export EXA_COLORS="da=1;34"
export LC_COLLATE="C"
export LC_CTYPE="en_US.UTF-8"
export LIBVIRT_DEFAULT_URI="qemu:///system"
export PATH="$PATH:$HOME/.local/bin:$HOME/repositories/tools/scripts"
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ssh-agent.socket


# E X T E R N A L
source "$HOME/.config/zsh/aliases"
source "$HOME/.config/zsh/tmux"
eval "$(direnv hook zsh)"
eval "$(starship init zsh)"
